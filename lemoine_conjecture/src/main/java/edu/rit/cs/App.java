/*
 * App.java
 * 
 * Version: 1.1.1
 * 
 * Revisions: 1.1
 * 
 */

package edu.rit.cs;
/**
 * This program implements lemoine conjecture sequentially as well as parallely
 * (using omp4j library).
 *  
 * @author Saloni Poddar
 *
 */
public class App 
{
	// static class variables
	static int lBound;
	static int hBound;
	static int pHigh=0,qHigh=0, n=0;
	 
	/**
	 * This function calculates the value of p and q for every given odd integer i.
	 * @param i Given Input odd Integer
	 */
	public static void lcCalculate(int i) {
		
		int p=0;
		double q=0;
		Prime.Iterator itr = new Prime.Iterator();
		while(i>(p=itr.next())) {        		
    		q=((i-p)/2.0);
    		if(q%1==0) {
    			if(Prime.isPrime((int)q)){
    				break;
    			}
    		}
    	}
    	if(pHigh<p || (pHigh==p && n<i) ){
    		pHigh=p;
    		n=i;
    		qHigh=(int)q;
    	}
	}
	
	/**
	 * This function implements lemoine conjecture sequentially and prints final output.
	 * 
	 */
    public static void seqLemoineConjecture() {
    	
        for (int i = lBound;i<=hBound;i+=2) {
        	lcCalculate(i);
        }
        System.out.println(n+" = "+pHigh+" + 2 * "+qHigh);
    }
    
    /**
     * This function implements lemoine conjecture parallely and prints final output.
     */
    public static void parLemoineConjecture() {
        
        //omp parallel for
    	for (int i = lBound;i<=hBound;i+=2) {
        	lcCalculate(i);
        }
    	System.out.println(n+" = "+pHigh+" + 2 * "+qHigh);
    }
    
    /**
	 * The main function.
	 * @param args Command line Arguments
	 */
    public static void main( String[] args ) {
    	
        lBound = Integer.parseInt(args[0]);
        hBound = Integer.parseInt(args[1]);
        
        //Checks the lower bound and upper bound conditions.
        if(lBound <=5 || lBound%2==0 || hBound%2==0) {
        	System.exit(0);
        }
 
        //Sequential Implementation
        System.out.println("Sequential Implementation :");
        long sSTime = System.currentTimeMillis();
        seqLemoineConjecture();
        long eSTime = System.currentTimeMillis();
        System.out.println("Total sequential runtime : "+ (eSTime-sSTime)+" ms");
        
        pHigh=0; 
        qHigh=0; 
        n=0;
        
        //Parallel Implementation
        System.out.println("Parallel Implementation :");
        long sPTime = System.currentTimeMillis();
        parLemoineConjecture();
        long ePTime = System.currentTimeMillis();
        System.out.println("Total parallel runtime : "+ (ePTime-sPTime)+" ms ");
    }
}
