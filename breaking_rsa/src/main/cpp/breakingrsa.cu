/*@author : Saloni Poddar */

#include <iostream>
#include <string> 
#include <iomanip>
#include <math.h>
#include <sstream>
#include <algorithm>

// Accessible by ALL CPU and GPU functions !!!
__managed__ unsigned long long int c;
__managed__ unsigned long long int m[3];
__managed__ bool flag;
__managed__ int count;


//Kernel function
__global__
void cuberoots(unsigned long long int n)
{

	// index = block index * number of threads per block + thread index
    int index = blockIdx.x * blockDim.x + threadIdx.x;
    // stride  = number threads per block * number of block per grid
    int stride = blockDim.x * gridDim.x;
	unsigned long long int val;

// brute force algo to calculate the cuberoots
	for (unsigned long long int a = index; a < n; a = a + stride) {
			val = (((a % n )* (a % n)) % n * (a % n)) % n;
			if (val == c) {
				atomicAdd(&count,1);
				m[count]=a;
		      		flag = true;
			}
	}
}

//host code
int main(int argc, char* argv[]) {

	//Check the number of commandline arguments
	if (argc!=3){
		std::cout << " Wrong Input Arguments!!! "<<std::endl;
		return 1 ;
	}
	
	//take inputs and initialize variable
	c = std::strtoull(argv[1],nullptr,10);
	unsigned long long int n = std::strtoull(argv[2],nullptr,10);
	flag = false;
	int blockSize = 256;
	int numBlocks = (n + blockSize - 1) / blockSize;
	count =-1;
	cuberoots<<<numBlocks, blockSize>>>(n);

	// Wait for GPU to finish before accessing on host
	cudaDeviceSynchronize();

	//print the output	
	if (flag == false){
		std::cout << "No cube roots of " << c << " (mod " << n << ")" <<std::endl;
	}
	else{
		std::sort(m,m+3);
		for (int i=0;i<3;i+=1){
			if (m[i]!=0)
			std::cout << m[i] <<"^3 =  "<< c <<" (mod "<< n << ")"<<std::endl;
		}
	}
	return 0;
}
