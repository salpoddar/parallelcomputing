/*
 * App.java
 *
 * Version: 1.1.1
 *
 * Revisions: 1.1
 *
 */

package edu.rit.cs;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.*;
import scala.Tuple2;


/**
 * This program finds US Census diversity index by map reduce parallel program using
 * Spark.
 *
 * @author Saloni Poddar
 *
 */
public class App {

    /**
     * The main function.
     * @param args
     */
    public static void main(String[] args) {
        // Creating Session and  a SparkConf that loads defaults from system properties and the classpath
        Logger.getLogger("org.apache").setLevel(Level.WARN);
        SparkConf sparkConf = new SparkConf().setMaster("local[4]").setAppName("US Census Diversity Index");
        SparkSession spark = SparkSession.builder().config(sparkConf).getOrCreate();

        //Creating Java context
        JavaSparkContext jsc = new JavaSparkContext(spark.sparkContext());

        //Reading file
        Dataset dataset = spark.read().option("header","true").option("delimiter",",").option("inferSchema", "true").csv("dataset/census.csv").filter("AGEGRP= 0");

        //Creating Java RDD object with required columns
        JavaRDD<Row> data = dataset.select("STNAME","CTYNAME","WA_MALE","WA_FEMALE","BA_MALE","BA_FEMALE","IA_MALE","IA_FEMALE",
                "AA_MALE","AA_FEMALE","NA_MALE","NA_FEMALE","TOM_MALE","TOM_FEMALE").toJavaRDD();

        //Creating Key-Value Pair
        JavaPairRDD<Row,Row> keyval = data.mapToPair(row -> new Tuple2<>(

                RowFactory.create(row.getString(0),row.getString(1)),
                RowFactory.create(
                        row.getInt(2)+row.getInt(3),
                        row.getInt(4)+row.getInt(5),
                        row.getInt(6)+row.getInt(7),
                        row.getInt(8)+row.getInt(9),
                        row.getInt(10)+row.getInt(11),
                        row.getInt(12)+row.getInt(13),
                        row.getInt(2)+row.getInt(3)+row.getInt(4)+row.getInt(5)+row.getInt(6)+
                                row.getInt(7)+row.getInt(8)+row.getInt(9)
                                +row.getInt(10)+row.getInt(11)+row.getInt(12)+row.getInt(13)
                )

        ));

        //Aggregating values based on keys
        JavaPairRDD<Row,Row> red = keyval.reduceByKey(
                (a,b)-> (RowFactory.create(
                        a.getInt(0)+b.getInt(0),
                        a.getInt(1)+b.getInt(1),
                        a.getInt(2)+b.getInt(2),
                        a.getInt(3)+b.getInt(3),
                        a.getInt(4)+b.getInt(4),
                        a.getInt(5)+b.getInt(5),
                        a.getInt(6)+b.getInt(6)

                )
                )
        );

        //Calculating Diversity Index
        JavaRDD<Row> diversity = red.map(
                row -> RowFactory.create(
                        row._1.getString(0),row._1.getString(1),
                        (
                                (1/((double) row._2.getInt(6) * (double) row._2.getInt(6)))
                                        *
                                        (
                                                (
                                                        ((double) row._2.getInt(0)*( (double)row._2.getInt(6) - (double)row._2.getInt(0)))+
                                                                ((double)row._2.getInt(1)*( (double)row._2.getInt(6) - (double)row._2.getInt(1)))+
                                                                ((double)row._2.getInt(2)*( (double)row._2.getInt(6) - (double)row._2.getInt(2)))+
                                                                ((double)row._2.getInt(3)*( (double)row._2.getInt(6) - (double)row._2.getInt(3)))+
                                                                ((double)row._2.getInt(4)*( (double)row._2.getInt(6) - (double)row._2.getInt(4)))+
                                                                ((double)row._2.getInt(5)*( (double)row._2.getInt(6) - (double)row._2.getInt(5)))
                                                )
                                        )
                        )
                )
        );
        //Printing Diversity
        for (Row line : diversity.collect())
            System.out.println(line);

        jsc.close();

    }
}
