<<<<<<< HEAD
package edu.rit.cs;


public class LargeTriangle
{

    public static void main( String[] args ) {
        int numPoints = 100;
        RandomPoints rndPoints = new RandomPoints(numPoints, 100, 142857);
        int idx=1;
        Point p;
        while(rndPoints.hasNext()) {
            p = rndPoints.next();
            System.out.println(idx+": x=" + p.getX() + " y=" + p.getY());
            idx++;
        }
    }
=======
/*
 * LargeTriangle.java
 * 
 * Version: 1.1.1
 * 
 * Revisions: 1.1
 * 
 */

package edu.rit.cs;
import mpi.*;
import java.nio.DoubleBuffer;
import java.nio.IntBuffer;

/**
 * This program finds largest triangle from random generated points parallely
 * (using openmpi).
 *  
 * @author Saloni Poddar
 *
 */

public class LargeTriangle
{
	/**
	 * This method calculates the Euclidean distance between given two points.
	 * @param point1 first point
	 * @param point2 second point
	 * @return Returns distance
	 */
	public static double eucDistance(Point point1, Point point2) {
		double x=0,y=0;
		double distance=0;
		x=point1.getX() - point2.getX();
		y=point1.getY() - point2.getY();
		distance=Math.sqrt(x*x+y*y);
		return distance;
	}

	/**
	 * This function checks if triangle can be formed from given sides.
	 * @param p first side
	 * @param q second side
	 * @param r third side
	 * @return Returns True if Triangle can be formed else false.
	 * 
	 */
	public static boolean isTriangle(double p, double q, double r) {
		boolean flag = true;
		flag = p+q>r && q+r>p && p+r>q;
		return flag;
	}
	
	/**
	 * The main function.
	 * @param args
	 * @throws MPIException
	 */
    public static void main( String[] args ) throws MPIException{
    	
    	long sTime = System.currentTimeMillis();
    	//Reading inputs
        int numPoints = Integer.parseInt(args[0]);
        int idx=0;
        int idx1=0,idx2=0,idx3=0;
        double maxArea = 0.0, area= 0.0;
        double p=0,q=0,r=0,s=0;
        Point [] pts = new Point[numPoints];
        
        //Initialising MPI
        MPI.Init(args);
        int size = MPI.COMM_WORLD.getSize();
        int rank = MPI.COMM_WORLD.getRank();
        int partition = numPoints/(size-1);
        IntBuffer[] sBuffer = new IntBuffer[size];
        DoubleBuffer[] rBuffer= new DoubleBuffer[size];
	
        //Preparing send buffer and receive buffer
        for(int i = 0;i<size;i++){
        	sBuffer[i] = MPI.newIntBuffer(2);
        	rBuffer[i] = MPI.newDoubleBuffer(4);
        }
        
        //generating random points
        RandomPoints rndPoints = new RandomPoints(numPoints, Integer.parseInt(args[1]), Integer.parseInt(args[2]));
        
        //adding pts in array
        while(rndPoints.hasNext()) {
            pts[idx] = rndPoints.next();
            idx++;
        }
       
        //looking for master process
        if(rank == 0){
            for(int i = 1; i<size;i++){
                sBuffer[i].put(0,(partition*(i-1)));
                if(i!=(size-1)){
                	sBuffer[i].put(1,partition*(i-1)+partition);
                }
                else{
                	sBuffer[i].put(1,pts.length);
                }
                MPI.COMM_WORLD.send(sBuffer[i],2,MPI.INT,i,0);
            }
            for (int i = 1;i<size;i++){
            	MPI.COMM_WORLD.recv(rBuffer[i],4,MPI.DOUBLE,i,0);
            	Double pt1Index = rBuffer[i].get(0);
            	Double pt2Index = rBuffer[i].get(1);
            	Double pt3Index = rBuffer[i].get(2);
            	Double calculatedArea = rBuffer[i].get(3);
            	if(maxArea < calculatedArea){
            		idx1 = pt1Index.intValue();
            		idx2 = pt2Index.intValue();
            		idx3 = pt3Index.intValue();		    
                    maxArea = calculatedArea;
               }
            }
        }
        //looking for the slave processes 
        else {
            MPI.COMM_WORLD.recv(sBuffer[rank],2,MPI.INT,0,0);
            for (int i=sBuffer[rank].get(0); i < sBuffer[rank].get(1); i++) {
            	for(int j=i+1;j<pts.length;j++) {
            		for (int k=j+1; k<pts.length;k++) {
            			p=eucDistance(pts[i],pts[j]);
            			q=eucDistance(pts[j],pts[k]);
            			r=eucDistance(pts[k],pts[i]);
            			if(isTriangle(p,q,r)) {
            				s=(p+q+r)/2;
            				area = Math.sqrt(s*(s-p)*(s-q)*(s-r));
            				if(area>maxArea) {
            					maxArea = area;
            					idx1=i;
            					idx2=j;
            					idx3=k;      					
            				}
            			}
            		}
            	}
            }
            rBuffer[rank].put(0,idx1);
            rBuffer[rank].put(1,idx2);
            rBuffer[rank].put(2,idx3);
            rBuffer[rank].put(3,maxArea);
            MPI.COMM_WORLD.send(rBuffer[rank],4,MPI.DOUBLE,0,0);
        }
        
        if (rank == 0) {
        	System.out.printf ("%d %.5g %.5g%n", idx1+1, pts[idx1].getX(),pts[idx1].getY());
            System.out.printf ("%d %.5g %.5g%n", idx2+1, pts[idx2].getX(),pts[idx2].getY());
            System.out.printf ("%d %.5g %.5g%n", idx3+1, pts[idx3].getX(),pts[idx3].getY());
            System.out.printf ("%.5g%n", maxArea);	
            long eTime = System.currentTimeMillis();
            System.out.println("Running Time: " + (eTime - sTime) + " ms");
        }
        
        MPI.Finalize();
    }

	
>>>>>>> 3c33bd029a73b7ecc70f0403260c607b5d0c7804
}
