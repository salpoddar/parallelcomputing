/*
 * LargeTriangleSeq.java
 * 
 * Version: 1.1.1
 * 
 * Revisions: 1.1
 * 
 */

package edu.rit.cs;

/**
 * This program finds largest triangle from random generated points sequentially.
 *  
 * @author Saloni Poddar
 *
 */

public class LargeTriangleSeq
{
	
	/**
	 * This method calculates the Euclidean distance between given two points.
	 * @param point1 first point
	 * @param point2 second point
	 * @return Returns distance
	 */
	public static double eucDistance(Point point1, Point point2) {
		double x=0,y=0;
		double distance=0;
		x=point1.getX() - point2.getX();
		y=point1.getY() - point2.getY();
		distance=Math.sqrt(x*x+y*y);
		return distance;
	}

	/**
	 * This function checks if triangle can be formed from given sides.
	 * @param p first side
	 * @param q second side
	 * @param r third side
	 * @return Returns True if Triangle can be formed else false.
	 * 
	 */
	public static boolean isTriangle(double p, double q, double r) {
		boolean flag = true;
		flag = p+q>r && q+r>p && p+r>q;
		return flag;
	}
	
	/**
	 * The main function.
	 * @param args
	 */
    public static void main( String[] args ) {
    	long sTime = System.currentTimeMillis();
        int numPoints = Integer.parseInt(args[0]);
        int idx=0;
        int idx1=0,idx2=0,idx3=0;
        double maxArea = 0.0, area= 0.0;
        double p=0,q=0,r=0,s=0;
        Point [] pts = new Point[numPoints];        
        RandomPoints rndPoints = new RandomPoints(numPoints, Integer.parseInt(args[1]), Integer.parseInt(args[2]));

        while(rndPoints.hasNext()) {
            pts[idx] = rndPoints.next();
            idx++;
        }
       
        for (int i=0; i <pts.length; i++) {
            	for(int j=i+1;j<pts.length;j++) {
            		for (int k=j+1; k<pts.length;k++) {
            			p=eucDistance(pts[i],pts[j]);
            			q=eucDistance(pts[j],pts[k]);
            			r=eucDistance(pts[k],pts[i]);
            			if(isTriangle(p,q,r)) {
            				s=(p+q+r)/2;
            				area = Math.sqrt(s*(s-p)*(s-q)*(s-r));
            				if(area>maxArea) {
            					maxArea = area;
            					idx1=i;
            					idx2=j;
            					idx3=k;      					
            				}
            			}
            		}
            	}
        }

        System.out.printf ("%d %.5g %.5g%n", idx1+1, pts[idx1].getX(),pts[idx1].getY());
        System.out.printf ("%d %.5g %.5g%n", idx2+1, pts[idx2].getX(),pts[idx2].getY());
        System.out.printf ("%d %.5g %.5g%n", idx3+1, pts[idx3].getX(),pts[idx3].getY());
        System.out.printf ("%.5g%n", maxArea);	
        long eTime = System.currentTimeMillis();
        System.out.println("Running Time: " + (eTime - sTime) + " ms");
        
    }

	
}
